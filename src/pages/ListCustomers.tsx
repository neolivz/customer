import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { listCustomers } from 'store/customer/listCustomers/listCustomersActions'
import { CustomerWithId } from 'store/types/Customer'
import { RootState } from 'store/types/Store'
import { CustomerCard } from 'components/CustomerCard/CustomerCard'
import { SearchBar } from 'components/SearchBar/SearchBar'
import { Success } from 'components/Success/Success'
import { Failure } from 'components/Failure/Failure'
import { Processing } from 'components/Processing/Processing'

import styled from 'styled-components'

const PageWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`

export const ListCustomer = () => {
    const dispatch = useDispatch()
    const [search, setSearch] = useState()
    useEffect(() => {
        dispatch(listCustomers(search))
    }, [search, dispatch])
    const customers = useSelector<RootState, CustomerWithId[] | undefined>(
        store => store.customers.customers,
    )
    const loading = useSelector<RootState, Boolean | undefined>(
        store => store.customers.loading,
    )
    const loadingComplete = useSelector<RootState, Boolean | undefined>(
        store => store.customers.loadingComplete,
    )
    const loadingFailed = useSelector<RootState, Boolean>(
        store => store.customers.loadingFailed,
    )
    return (
        <PageWrapper>
            <SearchBar search={search} onChange={setSearch} />
            {loading && <Processing>Loading Customers</Processing>}
            {loadingFailed && (
                <Failure>
                    Customer Loading Failed, Did you create any customers?
                </Failure>
            )}
            {loadingComplete && customers && (
                <div>
                    {customers.map(customer => (
                        <>
                            <CustomerCard customer={customer} />
                        </>
                    ))}
                </div>
            )}
            {loadingComplete && customers && customers.length === 0 && (
                <Success>No Customers in Database</Success>
            )}
        </PageWrapper>
    )
}
