import React, { useState, useCallback } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import {
    updateCustomer,
    updateCustomerFailure,
} from 'store/customer/updateCustomer/updateCustomerAction'
import { Customer, CustomerWithId } from 'store/types/Customer'
import { RootState } from 'store/types/Store'
import { CustomerDetails } from 'components/CustomerDetails/CustomerDetails'
import { Success } from 'components/Success/Success'
import { Failure } from 'components/Failure/Failure'
import { Processing } from 'components/Processing/Processing'

import styled from 'styled-components'

const PageWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`

const SaveButton = styled.button`
    height: 46px;
    width: 300px;
    background-color: #0093db;
    color: #fff;
    font-size: 14px;
    font-weight: 600;
`

const validateCustomer = (customer: Customer) => {
    const date = new Date()
    // TODO: Date is greater than today or at least 18 years old?
    // date.setFullYear(date.getFullYear() - 18)
    return (
        customer.firstName && customer.lastName && customer.dateOfBirth < date
    )
}

interface EditCustomer {
    customerId: string
}

export const EditCustomer = ({
    match: {
        params: { customerId },
    },
}: RouteComponentProps<EditCustomer>) => {
    console.log(customerId)
    const dispatch = useDispatch()
    const { customer: customerFromState } =
        useSelector<RootState, CustomerWithId | undefined>(state =>
            state.customers.customers.find(
                customer => customer.id === customerId,
            ),
        ) || {}
    const activeCustomer = useSelector<RootState, Customer | undefined>(
        store => store.customers.activeCustomer,
    )
    const success = useSelector<RootState, string | undefined>(
        store => store.customers.success,
    )
    const error = useSelector<RootState, string | undefined>(
        store => store.customers.error,
    )
    const saving = useSelector<RootState, Boolean>(
        store => store.customers.saving,
    )
    const [customer, setCustomer] = useState<Customer | undefined>(
        customerFromState,
    )
    const save = useCallback(() => {
        if (!customer) {
            return
        }
        if (validateCustomer(customer)) {
            dispatch(
                updateCustomer({
                    id: customerId,
                    customer,
                }),
            )
        } else {
            dispatch(
                updateCustomerFailure(
                    { id: customerId, customer },
                    'Validation Failed',
                ),
            )
        }
    }, [customer, customerId, dispatch])
    return (
        <PageWrapper>
            {activeCustomer === customer && success && (
                <Success>Saved Successfully</Success>
            )}
            {activeCustomer === customer && error && (
                <Failure>An Error Occured: {error}</Failure>
            )}
            {activeCustomer === customer && saving && (
                <Processing>Saving Customer</Processing>
            )}

            {customer && (
                <CustomerDetails customer={customer} onChange={setCustomer} />
            )}
            <SaveButton onClick={save}>Save</SaveButton>
        </PageWrapper>
    )
}
