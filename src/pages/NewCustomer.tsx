import React, { useState, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
    saveCustomer,
    saveCustomerFailure,
} from 'store/customer/createCustomer/createCustomerAction'
import { Customer } from 'store/types/Customer'
import { RootState } from 'store/types/Store'
import { CustomerDetails } from 'components/CustomerDetails/CustomerDetails'
import { Success } from 'components/Success/Success'
import { Failure } from 'components/Failure/Failure'
import { Processing } from 'components/Processing/Processing'

import styled from 'styled-components'

const PageWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`

const SaveButton = styled.button`
    height: 46px;
    width: 300px;
    background-color: #0093db;
    color: #fff;
    font-size: 14px;
    font-weight: 600;
`

const validateCustomer = (customer: Customer) => {
    const date = new Date()
    // TODO: Date is greater than today or at least 18 years old?
    // date.setFullYear(date.getFullYear() - 18)
    return (
        customer.firstName && customer.lastName && customer.dateOfBirth < date
    )
}

export const NewCustomer = () => {
    const dispatch = useDispatch()
    const activeCustomer = useSelector<RootState, Customer | undefined>(
        store => store.customers.activeCustomer,
    )
    const success = useSelector<RootState, string | undefined>(
        store => store.customers.success,
    )
    const error = useSelector<RootState, string | undefined>(
        store => store.customers.error,
    )
    const saving = useSelector<RootState, Boolean>(
        store => store.customers.saving,
    )
    const [customer, setCustomer] = useState<Customer>({
        firstName: '',
        lastName: '',
        dateOfBirth: new Date(),
    })
    const save = useCallback(() => {
        if (validateCustomer(customer)) {
            dispatch(saveCustomer(customer))
        } else {
            dispatch(saveCustomerFailure(customer, 'Validation Failed'))
        }
    }, [customer, dispatch])
    return (
        <PageWrapper>
            {activeCustomer === customer && success && (
                <Success>Saved Successfully</Success>
            )}
            {activeCustomer === customer && error && (
                <Failure>An Error Occured: {error}</Failure>
            )}
            {activeCustomer === customer && saving && (
                <Processing>Saving Customer</Processing>
            )}

            <CustomerDetails customer={customer} onChange={setCustomer} />
            <SaveButton onClick={save}>Save</SaveButton>
        </PageWrapper>
    )
}
