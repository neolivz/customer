import React from 'react'
import styled from 'styled-components'
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom'
import { NewCustomer } from 'pages/NewCustomer'
import { ListCustomer } from 'pages/ListCustomers'
import { EditCustomer } from 'pages/EditCustomer'

const AppLink = styled(Link)`
    padding-left: 10px;
`

const App: React.FC = () => {
    return (
        <Router>
            <div>
                <nav>
                    <AppLink to="/">Home</AppLink>
                    <AppLink to="/new">New Customer</AppLink>
                </nav>
                <Switch>
                    <Route exact path="/" component={ListCustomer} />
                    <Route exact path="/new" component={NewCustomer} />
                    <Route exact path="/list" component={ListCustomer} />
                    <Route
                        exact
                        path="/customer/:customerId"
                        component={EditCustomer}
                    />
                </Switch>
            </div>
        </Router>
    )
}

export default App
