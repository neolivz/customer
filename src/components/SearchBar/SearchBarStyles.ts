import styled from 'styled-components'

export const SearchContainer = styled.div`
    max-width: 100%;
    display: flex;
    flex-direction: column;
`

export const Row = styled.div`
    display: flex;
    flex-direction: row;
    padding: 10px;
`

export const Label = styled.div`
    display: flex;
    flex-direction: row;
    padding-right: 5px;
    justify-content: center;
    align-items: center;
`

export const Input = styled.input`
    min-height: 46px;
    width: 100%;
    padding-left: 10px;
    font-family: Montserrat, sans-serif;
    color: #001217;
    font-size: 14px;
    font-weight: 600;
    width: 250px;
`
