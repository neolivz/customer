import React, { useCallback } from 'react'

import { SearchContainer, Row, Label, Input } from './SearchBarStyles'

interface SearchBar {
    search: string | undefined
    onChange: (search: string | undefined) => any
}

export const SearchBar: React.FC<SearchBar> = ({ search, onChange }) => {
    const updateSearch = useCallback(
        (e: React.FormEvent<HTMLInputElement>) => {
            onChange(e.currentTarget.value)
        },
        [onChange],
    )
    return (
        <SearchContainer>
            <Row>
                <Label>Search</Label>
                <Input value={search} onChange={updateSearch} />
            </Row>
        </SearchContainer>
    )
}
