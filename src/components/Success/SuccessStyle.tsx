import styled from 'styled-components'

export const SuccessWrapper = styled.div`
    min-width: 300px;
    display: flex;
    min-height: 50px;
    padding: 10px;
    background: #00e676;
    justify-content: center;
    align-items: center;
    border-radius: 4px;
    color: #000;
`
