import React, { ReactNode } from 'react'
import { SuccessWrapper } from './SuccessStyle'

interface Success {
    children: ReactNode
}

export const Success: React.FC<Success> = ({ children }) => (
    <SuccessWrapper>{children}</SuccessWrapper>
)
