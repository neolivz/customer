import styled from 'styled-components'
import { Link } from 'react-router-dom'

export const CustomerCardContainer = styled.div`
    max-width: 100%;
    display: flex;
    flex-direction: row;
`

export const Column = styled.div`
    display: flex;
    flex-direction: column;
    padding: 10px;
    justify-content: center;
`

export const Label = styled.div`
    display: flex;
    flex-direction: column;
    padding-bottom: 5px;
`

export const Value = styled.div`
    display: flex;
    flex-direction: column;
`

export const SaveButton = styled(Link)`
    height: 46px;
    min-width: 100px;
    background-color: #0093db;
    color: #fff;
    font-size: 14px;
    font-weight: 600;
    justify-content: center;
    display: flex;
    align-items: center;
`

export const DeleteButton = styled.button`
    height: 46px;
    min-width: 100px;
    background-color: #ff3d00;
    color: #fff;
    font-size: 14px;
    font-weight: 600;
    justify-content: center;
    display: flex;
    align-items: center;
`

export const Input = styled.input`
    min-height: 46px;
    width: 100%;
    padding-left: 10px;
    font-family: Montserrat, sans-serif;
    color: #001217;
    font-size: 14px;
    font-weight: 600;
    width: 250px;
`
