import React, { useCallback } from 'react'
import { useDispatch } from 'react-redux'
import { CustomerWithId } from 'store/types/Customer'
import { deleteCustomer as deleteCustomerAction } from 'store/customer/deleteCustomer/deleteCustomerAction'

import {
    CustomerCardContainer,
    Column,
    Label,
    Value,
    SaveButton,
    DeleteButton,
} from './CustomerCardStyle'

interface CustomerCard {
    customer: CustomerWithId
}

export const CustomerCard: React.FC<CustomerCard> = ({ customer }) => {
    const dispatch = useDispatch()

    const deleteCustomer = useCallback(() => {
        dispatch(
            deleteCustomerAction({
                customer: customer.customer,
                id: customer.id,
            }),
        )
    }, [customer, dispatch])
    return (
        <CustomerCardContainer>
            <Column>
                <Label>First Name</Label>
                <Value>{customer.customer.firstName}</Value>
            </Column>
            <Column>
                <Label>Last Name</Label>
                <Value>{customer.customer.lastName}</Value>
            </Column>
            <Column>
                <Label>Date Of Birth</Label>
                <Value>
                    {customer.customer.dateOfBirth.getDate()}/
                    {customer.customer.dateOfBirth.getMonth() + 1}/
                    {customer.customer.dateOfBirth.getFullYear()}
                </Value>
            </Column>
            <Column>
                <SaveButton to={`customer/${customer.id}`}>Edit</SaveButton>
            </Column>
            <Column>
                <DeleteButton onClick={deleteCustomer}>Delete</DeleteButton>
            </Column>
        </CustomerCardContainer>
    )
}
