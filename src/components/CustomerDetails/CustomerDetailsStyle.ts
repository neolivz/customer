import styled from 'styled-components'

export const CustomerDetailsContainer = styled.div`
    max-width: 300px;
    display: flex;
    flex-direction: column;
`

export const Column = styled.div`
    display: flex;
    flex-direction: column;
    padding: 10px;
`

export const Label = styled.div`
    display: flex;
    flex-direction: column;
    padding-bottom: 5px;
`

export const Input = styled.input`
    min-height: 46px;
    width: 100%;
    padding-left: 10px;
    font-family: Montserrat, sans-serif;
    color: #001217;
    font-size: 14px;
    font-weight: 600;
    width: 250px;
`
