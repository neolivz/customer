import React, { useCallback, useState } from 'react'
import { Customer } from 'store/types/Customer'
import { DateSingleInput } from '@datepicker-react/styled'

import {
    CustomerDetailsContainer,
    Column,
    Label,
    Input,
} from './CustomerDetailsStyle'

interface CustomerDetails {
    customer: Customer
    onChange: (customer: Customer) => any
}

export const CustomerDetails: React.FC<CustomerDetails> = ({
    customer,
    onChange,
}) => {
    const [showDatePicker, setShowDatePicker] = useState()
    const updateFirstName = useCallback(
        (e: React.FormEvent<HTMLInputElement>) => {
            const firstName = e.currentTarget.value
            onChange({
                ...customer,
                firstName,
            })
        },
        [customer, onChange],
    )
    const updateLastName = useCallback(
        (e: React.FormEvent<HTMLInputElement>) => {
            const lastName = e.currentTarget.value
            onChange({
                ...customer,
                lastName,
            })
        },
        [customer, onChange],
    )
    const updateDateOfBirth = useCallback(
        ({ date: dateOfBirth, showDatepicker }) => {
            setShowDatePicker(showDatepicker)
            onChange({
                ...customer,
                dateOfBirth,
            })
        },
        [customer, onChange],
    )
    return (
        <CustomerDetailsContainer>
            <Column>
                <Label>First Name</Label>
                <Input value={customer.firstName} onChange={updateFirstName} />
            </Column>
            <Column>
                <Label>Last Name</Label>
                <Input value={customer.lastName} onChange={updateLastName} />
            </Column>
            <Column>
                <Label>Date Of Birth</Label>
                <DateSingleInput
                    onDateChange={updateDateOfBirth}
                    onFocusChange={setShowDatePicker}
                    date={customer.dateOfBirth}
                    showDatepicker={showDatePicker}
                />
            </Column>
        </CustomerDetailsContainer>
    )
}
