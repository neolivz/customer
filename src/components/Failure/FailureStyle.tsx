import styled from 'styled-components'

export const FailureWrapper = styled.div`
    min-width: 300px;
    display: flex;
    min-height: 50px;
    padding: 10px;
    background: #ff3d00;
    justify-content: center;
    align-items: center;
    border-radius: 4px;
    color: #fff;
`
