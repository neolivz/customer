import React, { ReactNode } from 'react'
import { FailureWrapper } from './FailureStyle'

interface Failure {
    children: ReactNode
}

export const Failure: React.FC<Failure> = ({ children }) => (
    <FailureWrapper>{children}</FailureWrapper>
)
