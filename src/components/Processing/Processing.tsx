import React, { ReactNode } from 'react'
import { ProcessingWrapper } from './ProcessingStyle'

interface Processing {
    children: ReactNode
}

export const Processing: React.FC<Processing> = ({ children }) => (
    <ProcessingWrapper>{children}</ProcessingWrapper>
)
