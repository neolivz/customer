import styled from 'styled-components'

export const ProcessingWrapper = styled.div`
    min-width: 300px;
    display: flex;
    min-height: 50px;
    padding: 10px;
    background: #00b0ff;
    justify-content: center;
    align-items: center;
    border-radius: 4px;
    color: #000;
`
