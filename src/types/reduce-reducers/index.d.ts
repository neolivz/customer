declare module 'reduce-reducers' {
    export default function reduceReducers<S, R, RT>(
        initialState: S,
        ...reducers: R[]
    ): RT
    export default function reduceReducers<S, R, RT>(...reducers: R[]): RT
}
