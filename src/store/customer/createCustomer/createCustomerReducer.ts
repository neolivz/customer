import { CustomerReducer } from '../customerReducer'
import {
    SAVE_CUSTOMER,
    SAVE_CUSTOMER_FAILURE,
    SAVE_CUSTOMER_SUCCESS,
} from './createCustomerConstatnts'

export const createCustomerReducer: CustomerReducer = (state, action) => {
    switch (action.type) {
        case SAVE_CUSTOMER: {
            return {
                ...state,
                success: undefined,
                error: undefined,
                saving: true,
                activeCustomer: action.customer,
            }
        }
        case SAVE_CUSTOMER_SUCCESS: {
            return {
                ...state,
                customers: [
                    ...state.customers,
                    {
                        id: action.id,
                        customer: action.customer,
                    },
                ],
                success: action.id,
                error: undefined,
                saving: false,
                activeCustomer: action.customer,
            }
        }
        case SAVE_CUSTOMER_FAILURE: {
            return {
                ...state,
                error: action.reason,
                success: undefined,
                saving: false,
                activeCustomer: action.customer,
            }
        }
    }
    return state
}
