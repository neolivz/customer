import { takeLatest, call, put, delay } from 'redux-saga/effects'
import { SAVE_CUSTOMER } from './createCustomerConstatnts'
import {
    saveCustomerFailure,
    saveCustomerSuccess,
} from './createCustomerAction'
import { SaveCustomerAction } from './createCustomerTypes'
import { createCustomer as createCustomerApi } from './createCustomerApi'

function* createCustomer(action: SaveCustomerAction) {
    // emulating network delay of 1.5 second
    yield delay(1500)
    const id = yield call(createCustomerApi, action.customer)
    if (id === '') {
        yield put(saveCustomerFailure(action.customer, 'API Failed to save'))
    } else {
        yield put(saveCustomerSuccess(action.customer, id))
    }
}

export function* createCustomerSaga(): Generator {
    yield takeLatest(SAVE_CUSTOMER, createCustomer)
}
