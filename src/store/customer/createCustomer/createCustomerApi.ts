import localforage from 'localforage'
import { Customer } from 'store/types/Customer'
import uuid from 'uuid/v1'
interface CreateCustomer {
    (customer: Customer): Promise<string>
}

export const createCustomer: CreateCustomer = async (customer: Customer) => {
    const id = uuid()
    try {
        await localforage.setItem(id, customer)
    } catch (e) {
        console.error(e)
        return ''
    }
    return id
}
