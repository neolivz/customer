import { Customer } from 'store/types/Customer'
import {
    SAVE_CUSTOMER,
    SAVE_CUSTOMER_FAILURE,
    SAVE_CUSTOMER_SUCCESS,
} from './createCustomerConstatnts'
import {
    SaveCustomer,
    SaveCustomerSuccess,
    SaveCustomerFailure,
} from './createCustomerTypes'

export const saveCustomer: SaveCustomer = (customer: Customer) => ({
    type: SAVE_CUSTOMER,
    customer,
})
export const saveCustomerSuccess: SaveCustomerSuccess = (
    customer: Customer,
    id: string,
) => ({
    type: SAVE_CUSTOMER_SUCCESS,
    id,
    customer,
})
export const saveCustomerFailure: SaveCustomerFailure = (
    customer: Customer,
    reason: string,
) => ({
    type: SAVE_CUSTOMER_FAILURE,
    reason,
    customer,
})
