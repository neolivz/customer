import { Customer } from 'store/types/Customer'
import { createCustomer } from '../createCustomerApi'
describe('Create Customer API', () => {
    beforeAll(() => {
        jest.mock('localforage')
        jest.mock('uuid/v1')
    })

    test('Succesfully Save Customer', async () => {
        const customer: Customer = {
            firstName: 'Success',
            lastName: 'Customer',
            dateOfBirth: new Date(),
        }
        const status: string = await createCustomer(customer)
        expect(status).toBe('newUUID')
    })
    test('Failure Save Customer', async () => {
        const customer: Customer = {
            firstName: 'Failure',
            lastName: 'Customer',
            dateOfBirth: new Date(),
        }
        const status: string = await createCustomer(customer)
        expect(status).toBe('')
    })
})
