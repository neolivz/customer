import { Customer } from 'store/types/Customer'
import {
    saveCustomer,
    saveCustomerFailure,
    saveCustomerSuccess,
} from '../createCustomerAction'
import {
    SAVE_CUSTOMER,
    SAVE_CUSTOMER_FAILURE,
    SAVE_CUSTOMER_SUCCESS,
} from '../createCustomerConstatnts'

describe('Create Customer Actions', () => {
    const customer: Customer = {
        dateOfBirth: new Date('01/01/1986'),
        firstName: 'FirstName',
        lastName: 'LastName',
    }

    const id = 'newId'

    test('Save Customer should call action with type SAVE_CUSTOMER, and the customer', () => {
        expect(saveCustomer(customer)).toEqual({
            type: SAVE_CUSTOMER,
            customer,
        })
    })
    test('Save Customer Success should call action with type SAVE_CUSTOMER, and the customer', () => {
        expect(saveCustomerSuccess(customer, id)).toEqual({
            type: SAVE_CUSTOMER_SUCCESS,
            id,
            customer,
        })
    })
    test('Save Customer Failure should call action with type SAVE_CUSTOMER, and the customer', () => {
        expect(saveCustomerFailure(customer, 'reason')).toEqual({
            type: SAVE_CUSTOMER_FAILURE,
            reason: 'reason',
            customer,
        })
    })
})
