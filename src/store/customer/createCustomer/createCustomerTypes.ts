import { Action, ActionWithResponse } from 'store/types/Actions'
import { Customer } from 'store/types/Customer'
import {
    SAVE_CUSTOMER,
    SAVE_CUSTOMER_FAILURE,
    SAVE_CUSTOMER_SUCCESS,
} from './createCustomerConstatnts'

type DataKey = 'customer'
type SuccessKey = 'id'
type ErrorKey = 'reason'

export type SaveCustomerAction = Action<SAVE_CUSTOMER, DataKey, Customer>

export type SaveCustomerActionSuccess = ActionWithResponse<
    SAVE_CUSTOMER_SUCCESS,
    DataKey,
    Customer,
    SuccessKey,
    string
>

export type SaveCustomerActionFailure = ActionWithResponse<
    SAVE_CUSTOMER_FAILURE,
    DataKey,
    Customer,
    ErrorKey,
    string
>

export type SaveCustomerActions =
    | SaveCustomerAction
    | SaveCustomerActionFailure
    | SaveCustomerActionSuccess

export interface SaveCustomer {
    (customer: Customer): SaveCustomerAction
}

export interface SaveCustomerSuccess {
    (customer: Customer, id: string): SaveCustomerActionSuccess
}

export interface SaveCustomerFailure {
    (customer: Customer, reason: string): SaveCustomerActionFailure
}
