export const SAVE_CUSTOMER = 'SAVE_CUSTOMER'
export type SAVE_CUSTOMER = 'SAVE_CUSTOMER'
export const SAVE_CUSTOMER_SUCCESS = 'SAVE_CUSTOMER_SUCCESS'
export type SAVE_CUSTOMER_SUCCESS = 'SAVE_CUSTOMER_SUCCESS'
export const SAVE_CUSTOMER_FAILURE = 'SAVE_CUSTOMER_FAILURE'
export type SAVE_CUSTOMER_FAILURE = 'SAVE_CUSTOMER_FAILURE'

export type ALL_SAVE_CUSTOMER =
    | SAVE_CUSTOMER
    | SAVE_CUSTOMER_SUCCESS
    | SAVE_CUSTOMER_FAILURE
