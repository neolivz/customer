import reduceReducers from 'reduce-reducers'

import { CustomerState } from 'store/types/Customer'

import { SaveCustomerActions } from './createCustomer/createCustomerTypes'
import { createCustomerReducer } from './createCustomer/createCustomerReducer'

import { ListCustomersActions } from './listCustomers/listCustomersTypes'
import { listCustomersReducer } from './listCustomers/listCustomersReducer'

import { UpdateCustomerActions } from './updateCustomer/updateCustomerTypes'
import { updateCustomerReducer } from './updateCustomer/updateCustomerReducer'

import { DeleteCustomerActions } from './deleteCustomer/deleteCustomerTypes'
import { deleteCustomerReducer } from './deleteCustomer/deleteCustomerReducer'

type ReducerAction =
    | SaveCustomerActions
    | ListCustomersActions
    | UpdateCustomerActions
    | DeleteCustomerActions

export type CustomerReducer = (
    state: CustomerState,
    action: ReducerAction,
) => CustomerState

export type CustomerReducerWithNoState = (
    state: CustomerState | undefined,
    action: ReducerAction,
) => CustomerState

const initState: CustomerState = {
    customers: [],
    loading: false,
    loadingComplete: false,
    loadingFailed: false,
    search: undefined,
    saving: false,
    error: undefined,
    success: undefined,
    activeCustomer: undefined,
    deleting: false,
    deletionComplete: false,
    deletionFailed: false,
}

export const customerReducer: CustomerReducerWithNoState = reduceReducers<
    CustomerState,
    CustomerReducer,
    CustomerReducerWithNoState
>(
    initState,
    listCustomersReducer,
    createCustomerReducer,
    updateCustomerReducer,
    deleteCustomerReducer,
)
