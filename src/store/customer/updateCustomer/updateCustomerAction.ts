import { CustomerWithId } from 'store/types/Customer'
import {
    UPDATE_CUSTOMER,
    UPDATE_CUSTOMER_FAILURE,
    UPDATE_CUSTOMER_SUCCESS,
} from './updateCustomerConstatnts'
import {
    UpdateCustomer,
    UpdateCustomerSuccess,
    UpdateCustomerFailure,
} from './updateCustomerTypes'

export const updateCustomer: UpdateCustomer = (customer: CustomerWithId) => ({
    type: UPDATE_CUSTOMER,
    customer,
})
export const updateCustomerSuccess: UpdateCustomerSuccess = (
    customer: CustomerWithId,
    id: string,
) => ({
    type: UPDATE_CUSTOMER_SUCCESS,
    id,
    customer,
})
export const updateCustomerFailure: UpdateCustomerFailure = (
    customer: CustomerWithId,
    reason: string,
) => ({
    type: UPDATE_CUSTOMER_FAILURE,
    reason,
    customer,
})
