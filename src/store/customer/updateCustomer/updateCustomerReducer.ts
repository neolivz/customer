import { CustomerReducer } from '../customerReducer'
import {
    UPDATE_CUSTOMER,
    UPDATE_CUSTOMER_FAILURE,
    UPDATE_CUSTOMER_SUCCESS,
} from './updateCustomerConstatnts'

export const updateCustomerReducer: CustomerReducer = (state, action) => {
    switch (action.type) {
        case UPDATE_CUSTOMER: {
            return {
                ...state,
                success: undefined,
                error: undefined,
                saving: true,
                activeCustomer: action.customer.customer,
            }
        }
        case UPDATE_CUSTOMER_SUCCESS: {
            return {
                ...state,
                customers: state.customers.map(customer => {
                    if (customer.id === action.customer.id) {
                        return action.customer
                    } else {
                        return customer
                    }
                }),
                success: action.id,
                error: undefined,
                saving: false,
                activeCustomer: action.customer.customer,
            }
        }
        case UPDATE_CUSTOMER_FAILURE: {
            return {
                ...state,
                error: action.reason,
                success: undefined,
                saving: false,
                activeCustomer: action.customer.customer,
            }
        }
    }
    return state
}
