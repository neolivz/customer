import { takeLatest, call, put, delay } from 'redux-saga/effects'
import { UPDATE_CUSTOMER } from './updateCustomerConstatnts'
import {
    updateCustomerFailure,
    updateCustomerSuccess,
} from './updateCustomerAction'
import { UpdateCustomerAction } from './updateCustomerTypes'
import { updateCustomer as updateCustomerApi } from './updateCustomerApi'

function* updateCustomer(action: UpdateCustomerAction) {
    // emulating network delay of 1.5 second
    yield delay(1500)
    const id = yield call(updateCustomerApi, action.customer)
    if (id === '') {
        yield put(updateCustomerFailure(action.customer, 'API Failed to save'))
    } else {
        yield put(updateCustomerSuccess(action.customer, id))
    }
}

export function* updateCustomerSaga(): Generator {
    yield takeLatest(UPDATE_CUSTOMER, updateCustomer)
}
