import { Action, ActionWithResponse } from 'store/types/Actions'
import { CustomerWithId } from 'store/types/Customer'
import {
    UPDATE_CUSTOMER,
    UPDATE_CUSTOMER_FAILURE,
    UPDATE_CUSTOMER_SUCCESS,
} from './updateCustomerConstatnts'

type DataKey = 'customer'
type SuccessKey = 'id'
type ErrorKey = 'reason'

export type UpdateCustomerAction = Action<
    UPDATE_CUSTOMER,
    DataKey,
    CustomerWithId
>

export type UpdateCustomerActionSuccess = ActionWithResponse<
    UPDATE_CUSTOMER_SUCCESS,
    DataKey,
    CustomerWithId,
    SuccessKey,
    string
>

export type UpdateCustomerActionFailure = ActionWithResponse<
    UPDATE_CUSTOMER_FAILURE,
    DataKey,
    CustomerWithId,
    ErrorKey,
    string
>

export type UpdateCustomerActions =
    | UpdateCustomerAction
    | UpdateCustomerActionFailure
    | UpdateCustomerActionSuccess

export interface UpdateCustomer {
    (customer: CustomerWithId): UpdateCustomerAction
}

export interface UpdateCustomerSuccess {
    (customer: CustomerWithId, id: string): UpdateCustomerActionSuccess
}

export interface UpdateCustomerFailure {
    (customer: CustomerWithId, reason: string): UpdateCustomerActionFailure
}
