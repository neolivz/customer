import localforage from 'localforage'
import { CustomerWithId } from 'store/types/Customer'

interface UpdateCustomer {
    (customer: CustomerWithId): Promise<string>
}

export const updateCustomer: UpdateCustomer = async (
    customer: CustomerWithId,
) => {
    try {
        console.log(customer.id, customer.customer)
        await localforage.setItem(customer.id, customer.customer)
        return customer.id
    } catch (e) {
        console.error(e)
        return ''
    }
}
