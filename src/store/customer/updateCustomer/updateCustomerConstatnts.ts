export const UPDATE_CUSTOMER = 'UPDATE_CUSTOMER'
export type UPDATE_CUSTOMER = 'UPDATE_CUSTOMER'
export const UPDATE_CUSTOMER_SUCCESS = 'UPDATE_CUSTOMER_SUCCESS'
export type UPDATE_CUSTOMER_SUCCESS = 'UPDATE_CUSTOMER_SUCCESS'
export const UPDATE_CUSTOMER_FAILURE = 'UPDATE_CUSTOMER_FAILURE'
export type UPDATE_CUSTOMER_FAILURE = 'UPDATE_CUSTOMER_FAILURE'

export type ALL_SAVE_CUSTOMER =
    | UPDATE_CUSTOMER
    | UPDATE_CUSTOMER_SUCCESS
    | UPDATE_CUSTOMER_FAILURE
