import { CustomerReducer } from '../customerReducer'
import {
    LIST_CUSTOMERS,
    LIST_CUSTOMERS_FAILURE,
    LIST_CUSTOMERS_SUCCESS,
} from './listCustomersConstatnts'

export const listCustomersReducer: CustomerReducer = (state, action) => {
    switch (action.type) {
        case LIST_CUSTOMERS: {
            return {
                ...state,
                search: action.search,
                loading: true,
                loadingComplete: false,
                loadingFailed: false,
            }
        }
        case LIST_CUSTOMERS_SUCCESS: {
            return {
                ...state,
                search: action.search,
                loading: false,
                loadingComplete: true,
                loadingFailed: false,
                customers: action.customers,
            }
        }
        case LIST_CUSTOMERS_FAILURE: {
            return {
                ...state,
                search: action.search,
                loading: false,
                loadingComplete: false,
                loadingFailed: true,
            }
        }
    }
    return state
}
