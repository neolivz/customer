import { CustomerWithId } from 'store/types/Customer'
import {
    LIST_CUSTOMERS,
    LIST_CUSTOMERS_FAILURE,
    LIST_CUSTOMERS_SUCCESS,
} from './listCustomersConstatnts'
import {
    DataKeyType,
    ListCustomers,
    ListCustomersFailure,
    ListCustomersSuccess,
} from './listCustomersTypes'

export const listCustomers: ListCustomers = (search: DataKeyType) => ({
    type: LIST_CUSTOMERS,
    search,
})
export const listCustomersSuccess: ListCustomersSuccess = (
    customers: CustomerWithId[],
    search: DataKeyType,
) => ({
    type: LIST_CUSTOMERS_SUCCESS,
    customers,
    search,
})
export const listCustomersFailure: ListCustomersFailure = (
    reason: string,
    search: DataKeyType,
) => ({
    type: LIST_CUSTOMERS_FAILURE,
    reason,
    search,
})
