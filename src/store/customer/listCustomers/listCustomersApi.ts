import localforage from 'localforage'
import { Customer, CustomerWithId } from 'store/types/Customer'
import { DataKeyType } from './listCustomersTypes'
interface ListCustomers {
    (search: DataKeyType): Promise<CustomerWithId[]>
}

export const listCustomers: ListCustomers = async (search: DataKeyType) => {
    try {
        const map: CustomerWithId[] = []
        await localforage.iterate<Customer, void>((customer, key) => {
            map.push({
                id: key,
                customer,
            })
        })
        if (search !== undefined) {
            return map.filter(
                ({ customer }) =>
                    customer.firstName
                        .toLowerCase()
                        .includes(search.toLowerCase()) ||
                    customer.lastName
                        .toLowerCase()
                        .includes(search.toLowerCase()),
            )
        }
        return map
    } catch (e) {
        console.error(e)
        return []
    }
}
