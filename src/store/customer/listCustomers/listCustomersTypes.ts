import { Action, ActionWithResponse } from 'store/types/Actions'
import { CustomerWithId } from 'store/types/Customer'
import {
    LIST_CUSTOMERS,
    LIST_CUSTOMERS_FAILURE,
    LIST_CUSTOMERS_SUCCESS,
} from './listCustomersConstatnts'

type DataKey = 'search'
type SuccessKey = 'customers'
type ErrorKey = 'reason'

export type DataKeyType = string | undefined

export type ListCustomersAction = Action<LIST_CUSTOMERS, DataKey, DataKeyType>

export type ListCustomersActionSuccess = ActionWithResponse<
    LIST_CUSTOMERS_SUCCESS,
    DataKey,
    DataKeyType,
    SuccessKey,
    CustomerWithId[]
>

export type ListCustomersActionFailure = ActionWithResponse<
    LIST_CUSTOMERS_FAILURE,
    DataKey,
    DataKeyType,
    ErrorKey,
    string
>

export type ListCustomersActions =
    | ListCustomersAction
    | ListCustomersActionFailure
    | ListCustomersActionSuccess

export interface ListCustomers {
    (search: DataKeyType): ListCustomersAction
}

export interface ListCustomersSuccess {
    (
        customers: CustomerWithId[],
        search: DataKeyType,
    ): ListCustomersActionSuccess
}

export interface ListCustomersFailure {
    (reason: string, search: DataKeyType): ListCustomersActionFailure
}
