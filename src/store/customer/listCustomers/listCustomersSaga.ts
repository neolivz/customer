import { takeLatest, call, put, delay, all } from 'redux-saga/effects'
import { CustomerWithId } from 'store/types/Customer'
import { LIST_CUSTOMERS } from './listCustomersConstatnts'
import {
    listCustomersFailure,
    listCustomersSuccess,
} from './listCustomersActions'
import { ListCustomersAction } from './listCustomersTypes'
import { listCustomers as listCustomersApi } from './listCustomersApi'

function* listCustomers(action: ListCustomersAction) {
    // emulating network delay of 1.5 second
    yield delay(1500)
    const customers: CustomerWithId[] = yield call(
        listCustomersApi,
        action.search,
    )
    if (customers.length > 0) {
        yield put(listCustomersSuccess(customers, action.search))
    } else {
        yield put(listCustomersFailure('API Failed to save', action.search))
    }
}

export function* listCustomersSaga(): Generator {
    yield all([
        takeLatest(LIST_CUSTOMERS, listCustomers),
        listCustomers({
            search: undefined,
            type: LIST_CUSTOMERS,
        }),
    ])
}
