import { Action, ActionWithResponse } from 'store/types/Actions'
import { CustomerWithId } from 'store/types/Customer'
import {
    DELETE_CUSTOMER,
    DELETE_CUSTOMER_FAILURE,
    DELETE_CUSTOMER_SUCCESS,
} from './deleteCustomerConstatnts'

type DataKey = 'customer'
type SuccessKey = 'id'
type ErrorKey = 'reason'

export type DeleteCustomerAction = Action<
    DELETE_CUSTOMER,
    DataKey,
    CustomerWithId
>

export type DeleteCustomerActionSuccess = ActionWithResponse<
    DELETE_CUSTOMER_SUCCESS,
    DataKey,
    CustomerWithId,
    SuccessKey,
    string
>

export type DeleteCustomerActionFailure = ActionWithResponse<
    DELETE_CUSTOMER_FAILURE,
    DataKey,
    CustomerWithId,
    ErrorKey,
    string
>

export type DeleteCustomerActions =
    | DeleteCustomerAction
    | DeleteCustomerActionFailure
    | DeleteCustomerActionSuccess

export interface DeleteCustomer {
    (customer: CustomerWithId): DeleteCustomerAction
}

export interface DeleteCustomerSuccess {
    (customer: CustomerWithId, id: string): DeleteCustomerActionSuccess
}

export interface DeleteCustomerFailure {
    (customer: CustomerWithId, reason: string): DeleteCustomerActionFailure
}
