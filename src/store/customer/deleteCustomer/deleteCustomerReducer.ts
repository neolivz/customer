import { CustomerReducer } from '../customerReducer'
import {
    DELETE_CUSTOMER,
    DELETE_CUSTOMER_FAILURE,
    DELETE_CUSTOMER_SUCCESS,
} from './deleteCustomerConstatnts'

export const deleteCustomerReducer: CustomerReducer = (state, action) => {
    switch (action.type) {
        case DELETE_CUSTOMER: {
            return {
                ...state,
                deletionComplete: false,
                deletionFailed: false,
                deleting: true,
                activeCustomer: action.customer.customer,
            }
        }
        case DELETE_CUSTOMER_SUCCESS: {
            return {
                ...state,
                customers: state.customers.filter(customer => {
                    if (customer.id === action.customer.id) {
                        return false
                    } else {
                        return true
                    }
                }),
                deletionComplete: true,
                deletionFailed: false,
                deleting: false,
                activeCustomer: action.customer.customer,
            }
        }
        case DELETE_CUSTOMER_FAILURE: {
            return {
                ...state,
                deletionComplete: true,
                deletionFailed: false,
                deleting: false,
                activeCustomer: action.customer.customer,
            }
        }
    }
    return state
}
