import { CustomerWithId } from 'store/types/Customer'
import {
    DELETE_CUSTOMER,
    DELETE_CUSTOMER_FAILURE,
    DELETE_CUSTOMER_SUCCESS,
} from './deleteCustomerConstatnts'
import {
    DeleteCustomer,
    DeleteCustomerSuccess,
    DeleteCustomerFailure,
} from './deleteCustomerTypes'

export const deleteCustomer: DeleteCustomer = (customer: CustomerWithId) => ({
    type: DELETE_CUSTOMER,
    customer,
})
export const deleteCustomerSuccess: DeleteCustomerSuccess = (
    customer: CustomerWithId,
    id: string,
) => ({
    type: DELETE_CUSTOMER_SUCCESS,
    id,
    customer,
})
export const deleteCustomerFailure: DeleteCustomerFailure = (
    customer: CustomerWithId,
    reason: string,
) => ({
    type: DELETE_CUSTOMER_FAILURE,
    reason,
    customer,
})
