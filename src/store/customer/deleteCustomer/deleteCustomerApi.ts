import localforage from 'localforage'
import { CustomerWithId } from 'store/types/Customer'

interface DeleteCustomer {
    (customer: CustomerWithId): Promise<string>
}

export const deleteCustomer: DeleteCustomer = async (
    customer: CustomerWithId,
) => {
    try {
        console.log(customer.id, customer.customer)
        await localforage.removeItem(customer.id)
        return customer.id
    } catch (e) {
        console.error(e)
        return ''
    }
}
