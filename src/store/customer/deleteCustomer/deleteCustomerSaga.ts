import { takeLatest, call, put, delay } from 'redux-saga/effects'
import { DELETE_CUSTOMER } from './deleteCustomerConstatnts'
import {
    deleteCustomerFailure,
    deleteCustomerSuccess,
} from './deleteCustomerAction'
import { DeleteCustomerAction } from './deleteCustomerTypes'
import { deleteCustomer as deleteCustomerApi } from './deleteCustomerApi'

function* deleteCustomer(action: DeleteCustomerAction) {
    // emulating network delay of 1.5 second
    yield delay(1500)
    const id = yield call(deleteCustomerApi, action.customer)
    if (id === '') {
        yield put(deleteCustomerFailure(action.customer, 'API Failed to save'))
    } else {
        yield put(deleteCustomerSuccess(action.customer, id))
    }
}

export function* deleteCustomerSaga(): Generator {
    yield takeLatest(DELETE_CUSTOMER, deleteCustomer)
}
