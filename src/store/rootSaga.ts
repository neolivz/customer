import { all } from 'redux-saga/effects'
import { createCustomerSaga } from './customer/createCustomer/createCustomerSaga'
import { listCustomersSaga } from './customer/listCustomers/listCustomersSaga'
import { updateCustomerSaga } from './customer/updateCustomer/updateCustomerSaga'
import { deleteCustomerSaga } from './customer/deleteCustomer/deleteCustomerSaga'

// Register all your watchers
export const rootSaga = function* root() {
    yield all([
        listCustomersSaga(),
        createCustomerSaga(),
        updateCustomerSaga(),
        deleteCustomerSaga(),
    ])
}
