import {
    createStore,
    combineReducers,
    applyMiddleware,
    Middleware,
} from 'redux'
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension'
import { rootSaga } from './rootSaga'
import { RootState } from 'store/types/Store'
import { customerReducer } from 'store/customer/customerReducer'

const rootReducer = combineReducers<RootState>({
    customers: customerReducer,
})

const sagaMiddleware = createSagaMiddleware()

export type AppState = ReturnType<typeof rootReducer>

export default function configureStore() {
    const middlewares: Array<Middleware> = [sagaMiddleware]
    const middleWareEnhancer = applyMiddleware(...middlewares)

    const store = createStore(
        rootReducer,
        composeWithDevTools(middleWareEnhancer),
    )

    sagaMiddleware.run(rootSaga)

    return store
}
