import { CustomerState } from './Customer'

export interface RootState {
    customers: CustomerState
}
