// DataKey of string, Data of type D
type ActionData<DK extends string, D> = {
    [K in DK]: D
}

// ResponseKey of string, Response of type R
type ActionResp<RK extends string, R> = {
    [K in RK]: R
}

export type NoDataAction<T extends string> = {
    type: T
}

export type Action<T extends string, DK extends string, D> = NoDataAction<T> &
    ActionData<DK, D>

export type NotDataActionWithResponse<
    T extends string,
    RK extends string,
    R
> = NoDataAction<T> & ActionResp<RK, R>

export type ActionWithResponse<
    T extends string,
    DK extends string,
    D,
    RK extends string,
    R
> = Action<T, DK, D> & ActionResp<RK, R>
