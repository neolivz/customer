export type Customer = {
    firstName: string
    lastName: string
    dateOfBirth: Date
}

export type CustomerWithId = {
    id: string
    customer: Customer
}

export interface CustomerState {
    customers: CustomerWithId[]
    loading: Boolean
    loadingComplete: Boolean
    loadingFailed: Boolean
    search: string | undefined
    saving: Boolean
    error: string | undefined
    success: string | undefined
    deleting: Boolean
    deletionComplete: Boolean
    deletionFailed: Boolean
    activeCustomer: Customer | undefined
}
