EPICS:
    1. Adding customers. (First name, last name and date of birth fields.)
    2. Editing customers.
    3. Deleting customers.
    4. Searching for a customer by partial name match (first or last name).

Tech Tasks:
    T-1: Add redux store
    T-2: Add redux-saga
    T-3: Add Jest with dummy test
    T-4: Add Styled-components
    T-5: Add React-Router
    T-6: Add Epic Feature Flags

Epic-1: Adding customers
    1. Create `<Customer />` component and add it page
    2. Create Action and invoke the action when Save Button is clicked
    3. Create API for adding customer and wire it with the action
    4. Create `<Success />` or `<Fail />` components and wire it to API response

Epic-2: Editing customers
    1. Create API for listing all the saved customers and create actions
    2. Create API to get the details of given saved customer and create actions
    3. Create Edit Customer Page and reuse `<Customer />` to render the details
    4. Create actions to update given customer and invoke then action when update button is clicked
    5. Create API for saving customer and wire it with the action
    6. Reused `<Success />` or `<Fail />` to show the API response

Epic-3: Deleting customers
    1. Add button in the listing page and details page to delete
    2. Create actions for deleting and invoke it when above buttons are clicked
    3. Create API for deleting a given customer and wire it with the action
    4. Reused `<Success />` or `<Fail />` to show the API response

Epic-4: Searching for a customer
    1. Add `<SearchBox />` in listing page.
    2. Update the list api to take optional search attribute
    3. Update the action to take the search attribute
    4. Invoke the action when ever the `<SearchBox >` changes

